#!/bin/bash
# Script to download the latest Raspbian image and copy it on SD 

DOWNLOAD_LINK="https://downloads.raspberrypi.org/raspbian_lite_latest"
IMG_ARCHIVE_NAME="*-raspbian-jessie-lite.zip"

DD_BS="8M"

# TODO: add possibility to pass the device path on command line
OUT_IF=""

# download the latest image
wget --trust-server-names -nc "$DOWNLOAD_LINK"

ARCHIVES=($(find . -name "$IMG_ARCHIVE_NAME" | sort -r))
LATEST_ARCHIVE=${ARCHIVES[0]}
LATEST_IMAGE="${LATEST_ARCHIVE%.*}.img"

# unzip the image
if [ ! -f "$LATEST_IMAGE" ]
then
    unzip "$LATEST_ARCHIVE"
fi

function choose_output_dev {
    # SD Card devices
    MMC_DEVS=($(ls /dev/mmcblk*))
    for i in $(seq 0 $((${#MMC_DEVS[@]} - 1)));
    do
        # strip all partition numbers
        MMC_DEVS[$i]=${MMC_DEVS[$i]%p*}
    done
    MMC_DEVS=$(echo "${MMC_DEVS[@]}" | tr ' ' '\n' | sort | uniq)

    # Solid state devices
    DISK_DEVS=($(ls /dev/sd*))
    for i in $(seq 0 $((${#DISK_DEVS[@]} - 1)));
    do
        # strip all partition numbers
        DISK_DEVS[$i]=${DISK_DEVS[$i]%[[:digit:]]*}
    done
    DISK_DEVS=$(echo "${DISK_DEVS[@]}" | tr ' ' '\n' | sort | uniq)
    
    # merge devices
    OUTPUT_DEVS=($MMC_DEVS $DISK_DEVS)

    while true;
    do
        echo "Choose device to which you want to install the image:"
        for i in $(seq 0 $((${#OUTPUT_DEVS[@]} - 1)));
        do
            echo "[$i] ${OUTPUT_DEVS[$i]}"
        done
        
        read -p "Device number: " DEVICE_NUM
        if [ "$DEVICE_NUM" -lt 0 -o "$DEVICE_NUM" -gt ${#OUTPUT_DEVS[@]} ]
        then
            echo "You MUST pass correct device number!"
            echo
            continue
        else
            eval "$1"="${OUTPUT_DEVS[$DEVICE_NUM]}"
            break
        fi
    done
}

choose_output_dev OUT_IF

DD_COMMAND="dd if=$LATEST_IMAGE of=$OUT_IF bs=$DD_BS"

RUN_DD=0
while true;
do
    read -p "Run the following command? '$DD_COMMAND' [yes/no] " RESPONSE
    if [ "$RESPONSE" != "yes" -a "$RESPONSE" != "no" ]
    then
        echo "You MUST type 'yes' or 'no'"
        echo
        continue
    else
        if [ "$RESPONSE" == "yes" ]
        then
            RUN_DD=1
        fi
        break
    fi
done

if [ "$RUN_DD" -eq 1 ]
then
    sudo $DD_COMMAND
    sync
    sync
fi

